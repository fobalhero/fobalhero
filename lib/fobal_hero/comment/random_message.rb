# encoding: utf-8
module FobalHero
  module Comment
    module RandomMessage
      module ClassMethods
        # TODO::Move this messages to a yaml file
        ACCEPT_MESSAGES = [
        	"voy!",
        	"yo voy!",
        	"me prendo",
        	"voooooy",
        	"Voy al arco",
        	"los voy a cagar a goles",
        	"Yo atajo!"
        ]

        DESERT_MESSAGES = [
        	"Nooo lo cagón que soy, no puedo ir",
        	"Me pinto hacer otra cosa, no voy nada",
        	"sorry loco no puedo ir",
        	"me cagaron, me toca pasear al perro",
        	"Me quedo en casa mirando tele, no voy.",
        	"Estoy con vómitos, no puedo ir.",
        	"Ceno con los suegros",
        	"nooo es el cumple de mi pibe",
        	"la próxima será.",
        	"no puedo ir, pero me prendo al asado",
          "tengo un bautismo de un primo, no voy nada.",
          "Me salio una cuca de emergencia en casa. Besis"
        ]

        def random_accept_message
        	return ACCEPT_MESSAGES.sample
        end

        def random_desert_message
        	return DESERT_MESSAGES.sample
        end

        def random_messages
          ACCEPT_MESSAGES + DESERT_MESSAGES
        end

        def find_previous_comment(comment)
          comment.commentable.comments.older_first.first
        end

      end

      def self.included(host_class)
        host_class.extend(ClassMethods)
        host_class.before_save :remove_previous_auto_message
      end

      def is_auto_generated?
        self.class.random_messages.include? self.body
      end

      private

      def remove_previous_auto_message
        last_comment = ::Comment.find_previous_comment(self)
        return nil if last_comment.nil?
        if self.user_id == last_comment.user_id && last_comment.is_auto_generated?
          last_comment.destroy
        end
      end

    end
  end
end