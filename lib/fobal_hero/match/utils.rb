module FobalHero
  module Match
    module Utils
      extend ActiveSupport::Concern
      
      included do
        def self.create_fake_match
          match = ::Match.new
          match.host = ::User.first
          match.is_draft = false
          match.play_at = Time.now + 7.days
          match.stadium_name = "Mi casa"
          match.team_one_name = 'Equipo A'
          match.team_two_name = 'Equipo B'
          match.match_type = 'eleven'
          match.save!
          users = ::User.order(:id).where('users.id <> ?', ::User.first.id).limit(23)
          users.each do |user|
            invite = ::InvitationToPlay.new
            invite.match_id = match.id
            invite.user_id = user.id
            invite.host_id = ::User.first.id
            invite.message = "un mensaje"
            invite.accepted = true
            invite.save!
          end
          users = ::User.order(:id).where('users.id <> ?', ::User.first.id).limit(2).offset(23)
          users.each do |user|
            invite = ::InvitationToPlay.new
            invite.match_id = match.id
            invite.user_id = user.id
            invite.host_id = ::User.first.id
            invite.message = "un mensaje"
            invite.accepted = false
            invite.save!
          end
          users = ::User.order(:id).where('users.id <> ?', ::User.first.id).limit(3).offset(25)
          users.each do |user|
            invite = ::InvitationToPlay.new
            invite.match_id = match.id
            invite.user_id = user.id
            invite.host_id = ::User.first.id
            invite.message = "un mensaje"
            invite.accepted = true
            invite.save!
            invite.accepted = nil
            invite.save!
          end
          comment = ::Comment.new
          comment.body = "Los vamos a hacer pija"
          comment.commentable = match
          comment.user_id = ::User.first.id
          comment.save!

          comment = ::Comment.new
          comment.body = "A tu hermana"
          comment.commentable = match
          comment.user_id = ::User.where('users.id <> ?', ::User.first.id).first.id
          comment.save!
        end
      end
    end
  end
end