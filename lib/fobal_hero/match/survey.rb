module FobalHero
  module Match
    module Survey
      extend ActiveSupport::Concern

      included do
        has_many :match_surveys, :through => :invitations_to_play
      end
      
      def someone_voted_yellow_card_for_player?(player)
        someone_voted_badge_for_player?(:yellow, player)
      end

      def someone_voted_badge_for_player?(type, player, exclude)
        q = ::MatchSurveyBadge.joins(:match_survey => {:invitation_to_play => :match}).where('match_survey_badges.user_id = ? AND badge_type = ?', player.id, type)
        q = q.where('invitations_to_play.user_id NOT IN (?)', exclude)
        q.exists?
      end

    end
  end
end