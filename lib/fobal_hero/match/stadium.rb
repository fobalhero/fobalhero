module FobalHero
  module Match
    module Stadium
      extend ActiveSupport::Concern

      included do
        belongs_to :stadium
        before_update :associate_with_stadium
      end

      def associate_with_stadium
        return nil if self.stadium_name.blank?
        possible_stadium = ::Stadium.where('lower(name) = ?', self.stadium_name.split.join(" ").downcase).first
        self.stadium = possible_stadium
      end
    end
  end
end