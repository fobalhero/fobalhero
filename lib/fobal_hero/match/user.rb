module FobalHero
  module Match
    module User
      extend ActiveSupport::Concern

      included do
        has_many :users, :through => :invitations_to_play do
          def going
            where(:invitations_to_play => { :accepted => true })
              .order('invitations_to_play.updated_at')
          end

          def not_going
            where(:invitations_to_play => { :accepted => false })
              .order('invitations_to_play.updated_at')
          end

          def undecided
            where(:invitations_to_play => { :accepted => nil })
              .order('invitations_to_play.id')
          end

          def starting(number_of_players)
            where(:invitations_to_play => { :accepted => true })
              .order('invitations_to_play.updated_at')
              .limit(number_of_players)
          end

          def substitutes(number_of_players)
            where(:invitations_to_play => { :accepted => true })
              .order('invitations_to_play.updated_at')
              .offset(number_of_players)
          end
        end

        belongs_to :host, :class_name => 'User'
        delegate :name, :to => :host, :allow_nil => true, :prefix => true

      end
    end
  end
end