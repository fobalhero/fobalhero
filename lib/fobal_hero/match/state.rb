module FobalHero
  module Match
    module State
      extend ActiveSupport::Concern

      included do
        after_initialize :update_old_play_at_when_draft
      end

      def update_old_play_at_when_draft
        # Set play_at to nil in case match is a draft and play_at date has already passed

        return unless is_draft
        return if play_at.nil?
        return if play_at > Time.now
        self.play_at = nil
      end

      # def close!
        #TODO let's continue this when we have a test that proves it's a good idea

        # Lets close the match, this means:
        #  calculate finale score,
        #  apply badges to players,
        #  update players statistics,
        #  apply rating to stadium,
        #  update stadium statistics,
        #  send notifications to players

        # calculate_final_score
        # deliver_badges_to_players
        # deliver_rating_to_stadium
        # self.is_closed = true
        # self.save!

        # notify_players_match_is_closed
      # end

      # def calculate_final_score
      #   self.team_one_score = self.team_one_proposed_score
      #   self.team_two_score = self.team_two_proposed_score
      # end

      # def deliver_badges_to_players
      #   self.players.each do |player|
      #     player.apply_badges_from_match(self)
      #   end
      # end

      # def deliver_rating_to_stadium
      #   if stadium.present?
      #     stadium.apply_rating_from_match(self)
      #   end
      # end

    end
  end
end