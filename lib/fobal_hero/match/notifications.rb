module FobalHero
  module Match
    module Notifications

      def notify_comment(comment)
        self.users.going.each do |user|
          next if user.id == comment.user_id
          notification = ::Notification.for_user(user).unread.where(:match_id => comment.commentable_id).where(:notification_type => ::Notification::TYPE_COMMENT).first || ::Notification.new
          notification.notification_type = ::Notification::TYPE_COMMENT
          notification.user_id = user.id
          notification.match_id = self.id
          notification.message = I18n.t(:new_comments_html, :scope => [:notifications])
          notification.save!
        end
      end

      def pending_notification?
        return false if self.is_draft?
        return true if self.match_changes.keys.any?
        return false
      end

      def notify_changes!
        recipients = self.users.going.pluck :email
        recipients += self.users.undecided.pluck :email
        MatchMailer.notify_match_changes(self.id, self.match_changes, recipients).deliver
        self.match_changes = {}
        self.save
      end
    end
  end
end