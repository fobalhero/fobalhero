module FobalHero
  module User
    module Facebook
      extend ActiveSupport::Concern

      included do
        extend ClassMethods
      end

      module ClassMethods
        def find_for_facebook_oauth(auth, signed_in_resource=nil)
          user = ::User.where(:provider => auth.provider, :uid => auth.uid).first
          if user.nil? and (user = ::User.where(:email => auth.info.email).first)
            user.provider = auth.provider
            user.uid = auth.uid
            user.avatar = URI.parse(auth.info.image.gsub("square", "large")) if user.avatar.nil?
            user.save!
          end
          if user.nil?
            user = ::User.new
            user.name = auth.info.name
            user.provider = auth.provider
            user.uid = auth.uid
            user.email = auth.info.email
            user.avatar = URI.parse(auth.info.image.gsub("square", "large"))
            user.password = ::Devise.friendly_token[0,20]
            user.skip_confirmation!
            user.save!
          end
          user
        end

        def new_with_session(params, session)
          super.tap do |user|
            if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
              user.email = data["email"] if user.email.blank?
            end
          end
        end
      end
    end
  end
end