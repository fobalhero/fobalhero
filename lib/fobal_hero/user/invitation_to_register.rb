module FobalHero
  module User
    module InvitationToRegister
      extend ActiveSupport::Concern

      included do
        has_many :invitations_to_register, :dependent => :destroy, :class_name => 'InvitationToRegister'
      end
    end
  end
end