module FobalHero
  module User
    module InvitationToPlay
      extend ActiveSupport::Concern

      included do
        extend ClassMethods
        has_many :matches, :through => :invitations_to_play
        has_many :invitations_to_play, :dependent => :destroy, :class_name => 'InvitationToPlay' do
          def accepted
            where(:accepted => true)
          end

          def declined
            where(:accepted => false)
          end

          def pending
            where(:accepted => nil)
          end
        end
      end

      module ClassMethods
        def invite_emails(emails, host)
          users = []
          emails.each do |email|
            user = ::User.find_by_email email
            if user.nil?
              user = ::User.new
              user.email = email
              user.password = ::Devise.friendly_token[0,20]
              user.skip_confirmation!
              next unless user.save
              ::InvitationToRegister.create :user_id => user.id, :host_id => host.id
            end
            ::Friendship.find_or_create_by_user_id_and_friend_id(:user_id => host.id, :friend_id => user.id)
            users << user
          end
          users
        end
      end
    end
  end
end