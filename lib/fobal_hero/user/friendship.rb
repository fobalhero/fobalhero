module FobalHero
  module User
    module Friendship
      extend ActiveSupport::Concern

      included do
        has_many :friendships, :dependent => :destroy, :foreign_key => 'user_id' ,:class_name => 'Friendship'
        has_many :own_friends, :through => :friendships, :class_name => "User", :uniq => :true, :source => :friend
        has_many :friendships_of, :dependent => :destroy, :foreign_key => 'friend_id' ,:class_name => 'Friendship'
        has_many :friends_of, :through => :friendships_of, :class_name => "User", :uniq => true, :source => :user
      end

      def friends
      	friends = (self.own_friends + self.friends_of).uniq
      	friends
      end

      def friend_ids
      	friend_ids = (self.own_friends.pluck('users.id') + self.friends_of.pluck('users.id')).uniq
      	friend_ids
      end
    end
  end
end