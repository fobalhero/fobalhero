module FobalHero
  module User
    module Match
      def invited?(match)
        self.invitations_to_play.where(:match_id => match.id).count > 0
      end

      def played?(match)
        self.invitations_to_play.where(:match_id => match.id, :accepted => true).count > 0
      end

      def goals(match)
        (invitation_to_play(match).match_survey.goals rescue 0) || 0
      end

      def invitation_to_play(match)
        self.invitations_to_play.where(:match_id => match.id).first
      end

      def organized_matches
        ::Match.where(:host_id => self.id)
      end

      # def apply_badges_from_match(match)
      #   invalid_goals_count = 0
      #   red_cards_count = 0
      #   yellow_cards_count = 0
      #   doping_count = 0
      #   man_of_the_match_count = 0
      #   cold_chest_count = 0
      #   betrayer_count = 0

      #   (match.users.going - self).each do |player|
      #     #player.
      #     ## Por acaaa!
      #   end
      # end
    end
  end
end