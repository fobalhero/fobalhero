class CustomFailure < Devise::FailureApp
  def redirect_url
    root_path
  end

  def respond
    if http_auth?
      http_auth
    else
      flash[:warning] = I18n.t(
        :unauthenticated_forgot_password,
        :url => new_user_password_path,
        :scope => [ :devise, :failure ]
      ).html_safe
      redirect
    end
  end
end