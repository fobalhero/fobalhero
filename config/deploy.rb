require "bundler/capistrano"

default_environment['GIT_SSL_NO_VERIFY'] = '1'
default_environment['RAILS_ENV'] = 'production'
default_environment['SENDGRID_PASSWORD'] = ENV['SENDGRID_PASSWORD']
default_environment['SENDGRID_USERNAME'] = ENV['SENDGRID_USERNAME']
default_environment['AWS_S3_BUCKET_NAME'] = ENV['AWS_S3_BUCKET_NAME']
default_environment['AWS_ACCESS_KEY_ID'] = ENV['AWS_ACCESS_KEY_ID']
default_environment['AWS_SECRET_ACCESS_KEY'] = ENV['AWS_SECRET_ACCESS_KEY']
default_environment['FACEBOOK_APP_ID'] = ENV['FACEBOOK_APP_ID']
default_environment['FACEBOOK_APP_SECRET'] = ENV['FACEBOOK_APP_SECRET']


role :web
role :app
role :db

set :application, "fobalhero"
set :repository,  "git@bitbucket.org:fobalhero/fobalhero.git"
set :scm, :git
set :user, "webapps"
set :deploy_to, "/opt/fobalhero"
set :rails_env,      "production"
set :passenger_port, 1082
set :passenger_cmd,  "passenger"
set :bundle_cmd, "bundle"
set :rvm_type, :system
set :rvm_ruby_string, '1.9.3@fobalhero'

before "deploy:assets:precompile", "deploy:create_symlinks"
after "deploy:create_symlinks", "deploy:back_label"
after "deploy:assets:symlink", "deploy:create_rvmrc_file"
after "deploy:setup", "deploy:create_uploads_dir"

desc 'configure servers for staging deployment, run `cap staging deploy`'
task :staging do
  server "pellegrini.gisworking.com", :app, :web, :db, :primary => true
end

namespace :deploy do
  task :start  do
    run "sudo /etc/init.d/passenger start fobalhero"
  end

  task :stop do
    run "sudo /etc/init.d/passenger stop fobalhero"
  end

  task :restart do
    run "sudo /etc/init.d/passenger restart fobalhero"
  end

  desc 'create backlabel file'
  task :back_label do
    run "cd #{current_release} && git rev-parse HEAD > public/build.txt"
    run "cd #{current_release} && date >> public/build.txt"
  end

  desc 'create .rvmrc file'
  task :create_rvmrc_file do
    run "cd #{current_release} && cp .rvmrc.example .rvmrc"
    run "cd #{current_release} && echo export SENDGRID_PASSWORD=#{default_environment['SENDGRID_PASSWORD']} >> .rvmrc"
    run "cd #{current_release} && echo export SENDGRID_USERNAME=#{default_environment['SENDGRID_USERNAME']} >> .rvmrc"
    run "cd #{current_release} && echo export AWS_S3_BUCKET_NAME=#{default_environment['AWS_S3_BUCKET_NAME']} >> .rvmrc"
    run "cd #{current_release} && echo export AWS_ACCESS_KEY_ID=#{default_environment['AWS_ACCESS_KEY_ID']} >> .rvmrc"
    run "cd #{current_release} && echo export AWS_SECRET_ACCESS_KEY=#{default_environment['AWS_SECRET_ACCESS_KEY']} >> .rvmrc"
    run "cd #{current_release} && echo export FACEBOOK_APP_ID=#{default_environment['FACEBOOK_APP_ID']} >> .rvmrc"
    run "cd #{current_release} && echo export FACEBOOK_APP_SECRET=#{default_environment['FACEBOOK_APP_SECRET']} >> .rvmrc"
  end

  desc 'create uploads dir'
  task :create_uploads_dir do
    run "#{try_sudo} mkdir #{shared_path}/system"
  end

  desc 'create symlinks'
  task :create_symlinks do
    run "ln -s #{shared_path}/system #{current_release}/system"
  end
end

# My god rvm requires this to be last or it doesnt work.
require "rvm/capistrano"