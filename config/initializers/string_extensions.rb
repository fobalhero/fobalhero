class String
	def is_integer?
		!!(self =~ /^[-+]?[0-9]+$/)
	end

	def is_positive_integer?
		!!(self =~ /^[+]?[0-9]+$/)
	end
end