FobalHero::Application.routes.draw do

  resources :friendships

  resources :stadiums

  post 'notifications/read'

  resources :invitations_to_register, :only => [:create] do
    member do
      get 'accept'
    end
  end

  resources :invitations_to_play, :only => [] do
    member do
      get 'accept'
      get 'decline'
    end
  end

  resources :matches, :except => [:destroy, :edit] do
    member do
      post :notify_changes
      post :invite_to_play
      post :post_comment
      get :comments
      post :i_played, :controller => :surveys
      post :i_didnt_play, :controller => :surveys
      post :i_agree_score, :controller => :surveys
      post :i_dont_agree_score, :controller => :surveys
      post :set_my_goals, :controller => :surveys
      post :set_score, :controller => :surveys
      post :set_stadium_stars, :controller => :surveys

      post :badge, :controller => :match_survey_badges, :action => :create
      delete :badge, :controller => :match_survey_badges, :action => :destroy
    end
  end

  devise_for :users, :controllers => {
    :omniauth_callbacks => 'users/omniauth_callbacks',
    :confirmations => 'confirmations',
    :passwords => 'passwords',
    :registrations => 'registrations'
  }
  resources :users, :only => [:edit, :update, :show]
  match '/admin/users' => 'users#index'
  match 'profile' => 'users#edit'

  devise_scope :user do
    get 'login', :to => 'devise/sessions#new'
    get 'logout', :to => 'devise/sessions#destroy'
  end

  authenticated do
    root :to => 'home#authenticated'
  end

  post 'register', :to => 'home#register'

  root :to => 'home#public'

end
