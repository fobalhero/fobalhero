class AddMatchChangesColumn < ActiveRecord::Migration
  def up
  	add_column :matches, :match_changes, :text
  end

  def down
  	remove_column :matches, :match_changes
  end
end
