class AddStadiumIdToMatches < ActiveRecord::Migration
  def up
    add_column :matches, :stadium_id, :integer
    rename_column :matches, :location, :stadium_name
  end

  def down
    remove_column :matches, :stadium_id, :integer
    rename_column :matches, :stadium_name, :location
  end
end
