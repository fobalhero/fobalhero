class AddEmailToStadiums < ActiveRecord::Migration
  def change
    add_column :stadiums, :email, :string
  end
end
