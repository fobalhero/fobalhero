class AddMatchIdToNotifications < ActiveRecord::Migration
  def change
    add_column :notifications, :match_id, :integer
  end
end
