class CreateMatchSurveyBadges < ActiveRecord::Migration
  def change
    create_table :match_survey_badges do |t|
      t.integer :user_id
      t.integer :match_survey_id
      t.string :badge_type

      t.timestamps
    end
  end
end
