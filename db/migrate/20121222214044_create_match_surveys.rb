class CreateMatchSurveys < ActiveRecord::Migration
  def change
    create_table :match_surveys do |t|
      t.integer :invitation_to_play_id
      t.boolean :played
      t.integer :team_one_score
      t.integer :team_two_score
      t.integer :goals

      t.timestamps
    end
  end
end
