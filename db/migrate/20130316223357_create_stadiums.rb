class CreateStadiums < ActiveRecord::Migration
  def change
    create_table :stadiums do |t|
      t.string :name
      t.string :phone_one
      t.text :description
      t.decimal :lat
      t.decimal :lon

      t.timestamps
    end
  end
end
