class AddLogoToStadiums < ActiveRecord::Migration
  def up
    add_attachment :stadiums, :logo
  end

  def down
    remove_attachment :stadiums, :logo
  end
end
