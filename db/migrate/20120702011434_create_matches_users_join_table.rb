class CreateMatchesUsersJoinTable < ActiveRecord::Migration
  def change
    create_table :matches_users, :id => false do |t|
      t.integer :match_id
      t.integer :user_id
    end
  end
end
