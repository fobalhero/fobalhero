class AddDraftToMatches < ActiveRecord::Migration
  def change
    add_column :matches, :is_draft, :boolean, :default => true
  end
end
