class CreateInvitationToRegisters < ActiveRecord::Migration
  def change
    create_table :invitations_to_register do |t|
      t.integer :host_id
      t.integer :user_id

      t.timestamps
    end
  end
end
