class CreateInvitationsToPlay < ActiveRecord::Migration
  def change
    create_table :invitations_to_play do |t|
      t.integer :match_id
      t.integer :user_id
      t.integer :host_id
      t.string :message
      t.string :comment
      t.boolean :accepted

      t.timestamps
    end
  end
end
