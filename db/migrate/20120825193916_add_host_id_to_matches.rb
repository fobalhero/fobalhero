class AddHostIdToMatches < ActiveRecord::Migration
  def change
    add_column :matches, :host_id, :integer
  end
end
