class DestroyMatchesUsersTable < ActiveRecord::Migration
  def up
    drop_table :matches_users
  end

  def down
    create_table :matches_users, :id => false do |t|
      t.integer :match_id
      t.integer :user_id
    end
  end
end
