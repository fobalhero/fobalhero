class AddStadiumStarsToMatchSurveys < ActiveRecord::Migration
  def change
    add_column :match_surveys, :stadium_stars, :integer
  end
end
