class AddTeamNamesToMatches < ActiveRecord::Migration
  def change
    add_column :matches, :team_one_name, :string
    add_column :matches, :team_two_name, :string
  end
end
