class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.datetime :play_at
      t.string :location

      t.timestamps
    end
  end
end
