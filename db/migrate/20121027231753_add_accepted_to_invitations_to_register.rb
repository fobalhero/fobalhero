class AddAcceptedToInvitationsToRegister < ActiveRecord::Migration
  def change
    add_column :invitations_to_register, :accepted, :boolean
  end
end
