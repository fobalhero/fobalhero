class AddPhoneTwoToStadiums < ActiveRecord::Migration
  def change
    add_column :stadiums, :phone_two, :string
  end
end
