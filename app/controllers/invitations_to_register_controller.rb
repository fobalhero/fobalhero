class InvitationsToRegisterController < ApplicationController
  before_filter :find_invitation, :require_no_authentication, :only => [:accept]
  skip_before_filter :authenticate_user!

  def create
    @emails = params[:invite][:emails].downcase.scan(/\b[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}\b/)
    @users = User.invite_emails(@emails, current_user)
  end

  def accept
    if @invitation.accepted?
      redirect_to profile_path, notice: I18n.t(:already_accepted, :scope => [:controllers, :invitation_to_register])
      return
    end
    @invitation.accepted!
    @invitation.user.prepare_to_set_password!
    redirect_to edit_user_password_path(:reset_password_token => @invitation.user.reset_password_token)
  end

private
  def find_invitation
    @invitation = InvitationToRegister.find params[:id]
    redirect_to root_path,
      :flash => { 
        error: t(:unknown_invitation, :scope => [:controllers, :invitation_to_register])
      } if @invitation.nil?
  end

end
