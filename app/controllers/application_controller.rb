class ApplicationController < ActionController::Base
  protect_from_forgery
  
  before_filter :authenticate_user!

  layout :layout_by_auth

  def layout_by_auth
  	if user_signed_in?
  		"logged"
  	else
  		"application"
  	end
  end

  def require_no_authentication
    redirect_to(root_path, :warning => I18n.t(:no_auth_required, :scope => [:controllers, :app])) if user_signed_in?
  end
  
end
