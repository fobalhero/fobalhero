class NotificationsController < ApplicationController
  def read
    Notification.for_user(current_user).where(:id => params[:notification_ids]).update_all(:read => true)
    head :ok
  end
end
