class HomeController < ApplicationController
	before_filter :authenticate_user!, :except => [:public, :register]

	def public
    @new_user = User.new
  end

  def authenticated
  end

  def register
		@new_user = User.new
    @new_user.email = params[:user][:email] if params[:user] and params[:user][:email]
    @new_user.password = params[:user][:password] if params[:user] and params[:user][:password]
    @new_user.password_confirmation = @new_user.password

    respond_to do |format|
      if @new_user.save
        format.html { redirect_to root_path, notice: I18n.t(:signed_up_but_unconfirmed, :scope => [:devise, :registrations]) }
        format.json { render json: @new_user, status: :created, location: @new_user }
      else
        format.html { render action: "public" }
        format.json { render json: @new_user.errors, status: :unprocessable_entity }
      end
    end
  end

end
