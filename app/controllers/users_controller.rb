class UsersController < ApplicationController
  def index
    @active = User.last_logged_first
  	@inactive = User.never_logged
  end

  def show
    @user = User.find params[:id]
  end

  def edit
  	@user = current_user
  end

  def update
  	@user = current_user

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to user_path(@user), notice: 'Cambios guardados' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
end
