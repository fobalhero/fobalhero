class InvitationsToPlayController < ApplicationController
  before_filter :find_invitation, :check_invitation_owner, :check_match_play_at_is_future

  def accept
    match = @invitation.match
    return redirect_to(match_path(match)) if @invitation.accepted?

    @invitation.accepted!
    redirect_to match_path(match),
      :flash => {
        :success => t(:invitation_accepted, :scope => [:controllers, :invitation_to_play])
      }
  end

  def decline
    match = @invitation.match
    return redirect_to(match_path(match)) if @invitation.declined?

    @invitation.declined!
    redirect_to match_path(match),
      :flash => { 
        :success => t(:invitation_declined, :scope => [:controllers, :invitation_to_play])
      }
  end

private

  def check_match_play_at_is_future
    if @invitation.match.already_played?
      redirect_to match_path(@invitation.match), :flash => {
        error: t(:read_only_match, :scope => [:controllers, :invitation_to_play])
      }
      return
    end
  end

  def check_invitation_owner
    if @invitation.user != current_user
      redirect_to root_path,
        :flash => { 
          error: t(:not_invited_user, :scope => [:controllers, :invitation_to_play])
        }
    end
  end

  def find_invitation
    @invitation = InvitationToPlay.find params[:id]
    redirect_to root_path,
      :flash => { 
        error: t(:unknown_invitation, :scope => [:controllers, :invitation_to_play])
      } if @invitation.nil?
  end
end
