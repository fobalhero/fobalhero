class SurveysController < ApplicationController
  before_filter :find_match, :find_invitation_to_play, :find_match_survey

  def i_played
    @survey.played = true
    @survey.save
    if proposed_score?
      render 'matches/survey/proposed_score_step', :locals => { :match => @match, :survey => @survey }
    else
      render 'matches/survey/score_step', :locals => { :match => @match, :survey => @survey }
    end
  end

  def i_didnt_play
    @survey.played = false
    @survey.save
    render 'matches/survey/review_step', :locals => { :match => @match, :survey => @survey }
  end

  def i_agree_score
    @survey.team_one_score = @match.team_one_proposed_score
    @survey.team_two_score = @match.team_two_proposed_score

    respond_to do |format|
      if @survey.save
        format.js { render 'matches/survey/goals_step', :locals => { :match => @match, :survey => @survey } }
      else
        format.js { render 'matches/survey/score_step', :locals => { :match => @match, :survey => @survey } }
      end
    end
  end

  def i_dont_agree_score
    render 'matches/survey/score_step', :locals => { :match => @match, :survey => @survey }
  end

  def set_score
    @survey.team_one_score = params[:team_one_score]
    @survey.team_two_score = params[:team_two_score]

    respond_to do |format|
      if @survey.save
        format.js { render 'matches/survey/goals_step', :locals => { :match => @match, :survey => @survey } }
      else
        format.js { render 'matches/survey/score_step', :locals => { :match => @match, :survey => @survey } }
      end
    end
    
  end

  def set_my_goals
    @survey.goals = params[:my_goals] || 0

    respond_to do |format|
      if @survey.save
        format.js { render 'matches/survey/qualify_step', :locals => { :match => @match, :survey => @survey } }
      else
        format.js { render 'matches/survey/goals_step', :locals => { :match => @match, :survey => @survey } }
      end
    end
  end

  def set_stadium_stars
    @survey.stadium_stars = params[:stars] || 1
    @survey.save
    head :ok
  end

private

  def proposed_score?
    (@match.match_surveys - [@survey]).each do |survey|
      return true if survey.team_one_score.present?
    end
    false
  end

  def find_match
    @match = Match.find(params[:id])
  end

  def find_invitation_to_play
    @invitation_to_play = current_user.invitation_to_play(@match)
  end

  def find_match_survey
    @survey = @invitation_to_play.match_survey || @invitation_to_play.create_match_survey
  end
end
