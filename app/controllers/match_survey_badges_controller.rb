class MatchSurveyBadgesController < SurveysController

  def create
    MatchSurveyBadge.find_or_create_by_match_survey_id_and_user_id_and_badge_type(@survey.id, params[:player_id], params[:type])
    render :partial => 'matches/survey/badges/toggle'
  end

  def destroy
    MatchSurveyBadge.where('match_survey_id = ? AND user_id = ? AND badge_type = ?', @survey.id, params[:player_id], params[:type]).delete_all
    render :partial => 'matches/survey/badges/toggle'
  end

end