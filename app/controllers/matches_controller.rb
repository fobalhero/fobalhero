class MatchesController < ApplicationController

  before_filter :find_match, :find_friends,  :only => [
    :show,
    :edit,
    :update,
    :invite_to_play,
    :notify_changes
  ]
  before_filter :check_match_play_at_is_future, :only => [
    :update,
    :invite_to_play
  ]

  def index
    @recent_matches = Match.pending_review(current_user)
    @upcoming_matches = Match.upcoming(current_user)
    @past_matches = Match.past(current_user)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @matches }
    end
  end

  def show
    initialize_comment
  end

  def new
    @match = Match.draft(current_user) || Match.new
    @match.host = current_user
    @match.match_type = @match.match_type || :five
    @match.play_at = Time.now + 2.days
    @match.save!

    respond_to do |format|
      format.html { redirect_to match_path @match }
      format.json { render json: @match }
    end
  end

  def create
    @match = Match.new(params[:match])

    respond_to do |format|
      if @match.save
        format.html { redirect_to @match, notice: 'Partido creado' }
        format.json { render json: @match, status: :created, location: @match }
      else
        format.html { render action: "new" }
        format.json { render json: @match.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @match.update_attributes(params[:match])
        format.html { redirect_to match_path(@match), notice: 'Cambios guardados' }
        format.json { head :no_content }
        format.js { render :nothing => true }
      else
        format.html { render action: "show" }
        format.js { render json: @match.errors, status: :unprocessable_entity }
      end
    end
  end

  def invite_to_play
    @emails = params[:emails].downcase.scan(/\b[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}\b/)
    @users = extract_users
    @match = Match.find params[:id]
    @users += User.invite_emails(@emails, current_user)
    InvitationToPlay.invite(@users, @match, current_user, params[:comment])
    @match.save
    render json: @users, :only => [:id, :name], :methods => [:avatar_thumb]
  end

  def post_comment
    @match = Match.find params[:id]
    @comment = Comment.new params[:comment]
    @comment.user = current_user
    @comment.commentable = @match
    @comment.save!

    #TODO not need application notifications just yet
    # @match.notify_comment(@comment)
    respond_to do |format|
      format.html { redirect_to match_path(@match) + "#comment_#{@comment.id}" }
      format.js
    end
  end

  def comments
    @comments = Match.find(params[:id]).comments.older_first
    @comments = @comments.where('comments.id > ?', params[:comment_id]) if params[:comment_id].present?
    @prev_comment = (params[:comment_id].present? ? Comment.find(params[:comment_id]) : @comments.first) rescue @comments.first
    @comment_side = params[:side].present? ? params[:side].to_sym : :left
  end

  def notify_changes

    respond_to do |format|
      if @match.pending_notification? & @match.notify_changes!
        format.html {head :ok}
      else
        format.html {head :bad_request}
      end
    end
  end

private

  def extract_users
    return User.find params[:user_ids].split(",") if params[:user_ids].present?
    return []
  end

  def initialize_comment
    @new_comment = Comment.new
    @new_comment.commentable = @match
    @new_comment.user = current_user
    @comment_side = :left
  end

  def find_match
    @match = Match.find params[:id]
  end

  def check_match_play_at_is_future
    if @match.already_played?
      redirect_to match_path(@match), :flash => {
        error: t(:read_only_match, :scope => [:controllers, :matches])
      }
      return
    end
  end

  def find_friends
    @friends = current_user.friends - @match.users
  end
end
