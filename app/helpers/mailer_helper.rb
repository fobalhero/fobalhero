module MailerHelper
  def match_change(label, value)
    label = I18n.t(label, :scope => [:emails, :bodies, :matches])
    if value.class == Time
      value = ActiveSupport::TimeWithZone.new value, Time.zone
      value = I18n.l(value, :format => :tiny)
    end
    return label + " " + value.to_s
  end

  def subject_for_match_changes(changes)
    if changes.keys.count > 1
      subject = I18n.t(:multiple_match_changes,
        :scope => [:emails, :subjects, :matches], :host_name => @host.name)
    elsif changes.keys.first == "play_at"
      subject = I18n.t(:match_date_changes,
        :scope => [:emails, :subjects, :matches], :host_name => @host.name)
    elsif changes.keys.first == "stadium_name"
      subject = I18n.t(:match_stadium_changes,
        :scope => [:emails, :subjects, :matches], :host_name => @host.name)
    else
      subject = I18n.t(:multiple_match_changes,
        :scope => [:emails, :subjects, :matches], :host_name => @host.name)
    end
    return subject
  end

end