module NotificationsHelper
  def display_notification(notification)
    content_tag :li, :class => "notification #{notification.read? ? 'read' : 'unread'}", :data => { :notification_id => notification.id } do
      link_to notification.message.html_safe, match_path(notification.match_id)
    end
  end
end