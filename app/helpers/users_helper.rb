module UsersHelper
  def display_badges_for(user, match)
    presenter = UserPresenter.new(user, self)
    content_tag :ul, :class => 'badges' do
      badges = "".html_safe
      badges += presenter.goals_badge(match).html_safe
      badges += presenter.yellow_card_badge(match).html_safe
      badges += presenter.red_card_badge(match).html_safe
      badges += presenter.doping_badge(match).html_safe
      badges += presenter.man_of_the_match_badge(match).html_safe
      badges += presenter.cold_chest_badge(match).html_safe
      badges += presenter.betrayer_badge(match).html_safe
      badges
    end
  end

  def organized_matches(user)
    user.organized_matches.map { |match| link_to "#{match.id}", match }.join(', ').html_safe
  end

  def friend_names(user)
    user.friends.map { |friend| friend.name }.join(', ').html_safe
  end
end
