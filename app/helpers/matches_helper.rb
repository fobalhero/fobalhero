module MatchesHelper

  def calculate_comment_side(current_comment, prev_comment)
    return @comment_side if current_comment.user == prev_comment.user
    @comment_side = (@comment_side == :left) ? :right : :left
    @comment_side
  end

  def display_details_for(match = @match)
    presenter = MatchPresenter.new(match, self)
    if block_given?
      yield presenter
    else
      presenter
    end
  end

  def display_survey_for(match = @match)
    presenter = MatchSurveyPresenter.new(match, current_user, self)
    if block_given?
      yield presenter
    else
      presenter
    end
  end

  def display_survey_badges_for(player, survey)
    content_tag :ul, :class => 'badges' do
      badges = "".html_safe
      badges += display_badge(player, survey, :fake_goals)
      badges += display_badge(player, survey, :yellow_card)
      badges += display_badge(player, survey, :red_card)
      badges += display_badge(player, survey, :doping)
      badges += display_badge(player, survey, :man_of_the_match)
      badges += display_badge(player, survey, :cold_chest)
      badges += display_badge(player, survey, :betrayer)
      badges
    end
  end

  def display_badge(player, survey, type)
    if type.to_sym == :fake_goals
      display_fake_goals_badge(player, survey)
    else
      did_vote_badge = survey.did_vote_badge_for_player?(type, player, [current_user])
      someone_voted_badge = survey.invitation_to_play.match.someone_voted_badge_for_player?(type, player, [current_user])
      content_tag :li, :class => "#{type.to_s.gsub('_', '-')}#{' voted' if did_vote_badge}#{' other-voted' if someone_voted_badge}#{' disabled' unless someone_voted_badge or did_vote_badge}" do
        link_to "", badge_match_path(survey.invitation_to_play.match, :type => type, :player_id => player.id), :method => (did_vote_badge ? :delete : :post), :remote => true, :title => t(type, :scope => [:badges])
      end
    end
  end

  def display_fake_goals_badge(player, survey)
    player_survey = MatchSurvey.by_user(player).by_match(survey.match).first
    return if player_survey.nil?
    return if player_survey.goals.nil?
    return unless player_survey.goals > 0
    did_vote_badge = survey.did_vote_badge_for_player?(:fake_goals, player, [current_user])
    someone_voted_badge = survey.invitation_to_play.match.someone_voted_badge_for_player?(:fake_goals, player, [current_user])
    content_tag :li, :class => "fake-goals#{' voted' if did_vote_badge}#{' other-voted' if someone_voted_badge}#{' disabled' unless someone_voted_badge or did_vote_badge}" do
      link_to player_survey.goals.to_s, badge_match_path(survey.invitation_to_play.match, :type => :fake_goals, :player_id => player.id), :method => (did_vote_badge ? :delete : :post), :remote => true, :title => t(:fake_goals, :scope => [:badges])
    end
  end

  def match_state(match)
    return "Cerrado" if match.is_closed?
    return "Evaluando resultado" if match.already_played?
    return "No definido" if match.play_at.nil?
    return "Por jugar"
  end

  def editable_stadium_field(name, object)
    raise "Method deprecated"
    resource_name = object.class.to_s.underscore

    content = content_tag(:span, :class => (object[name].present? ? "visible" : "hidden") + " editable-label") do
      link_to_unless object.stadium.nil?, object[name], hash_for_stadium_path(:id => object.stadium)
    end +
    content_tag(:span, :class => (object[name].present? ? "hidden" : "visible") + " editable-field") do
      text_field_tag("#{resource_name}[#{name}]", nil, :class => "input-mini", :data => { :provide => "typeahead", :items => "4", :source => stadiums_for_select.to_s }, :autocomplete => 'off')
    end
    content.html_safe
  end

  def stadiums_for_select
    Stadium.pluck(:name)
  end

  def match_date(match)
    return "Sin fecha" if match.play_at.nil?
    return l(match.play_at.to_date, :format => :short)
  end
end
