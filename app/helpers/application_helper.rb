module ApplicationHelper

  def header_not_required?
    params[:controller] == 'home' and params[:action] == 'authenticated'
  end

  def safe_controller_name
    params[:controller].gsub("/", "_")
  end

  def safe_action_name
    "action_#{params[:action]}"
  end

  def editable_text_field(name, object)
    resource_name = object.class.to_s.underscore

    content = content_tag(:span, :class => (object[name].present? ? "visible" : "hidden") + " editable-label") do
      object[name]
    end +
    content_tag(:span, :class => (object[name].present? ? "hidden" : "visible") + " editable-field") do
      text_field_tag("#{resource_name}[#{name}]", object[name], :placeholder => I18n.t("#{name.to_s}", :scope => [:activerecord, :attributes, object.class.to_s.underscore.to_sym], :default => name.to_s))
    end
    content.html_safe
  end

  def editable_date_field(name, object)
    resource_name = object.class.to_s.underscore

    content = content_tag(:span, :class => (object[name].present? ? "visible" : "hidden") + " editable-label") do
      object[name].nil? ? nil : I18n.l(object[name].to_date, :format => :default)
    end +
    content_tag(:span, :class => (object[name].present? ? "hidden" : "visible") + " editable-field") do
      proposed_date = Date.today + 1.day
      text_field_tag("#{name}_date", I18n.l(object[name].nil? ? proposed_date : object[name].to_date, :format => :default), { :class => 'input-small' }) +
      hidden_field_tag("#{resource_name}[#{name}(1i)]", object[name].nil? ? proposed_date.year : object[name].year) +
      hidden_field_tag("#{resource_name}[#{name}(2i)]", object[name].nil? ? proposed_date.month : object[name].month) +
      hidden_field_tag("#{resource_name}[#{name}(3i)]", object[name].nil? ? proposed_date.day : object[name].day)
    end
    content.html_safe
  end

  def editable_time_field(name, object)
    resource_name = object.class.to_s.underscore

    content = content_tag(:span, :class => (object[name].present? ? "visible" : "hidden") + " editable-label") do
      object[name].nil? ? nil : I18n.l(object[name], :format => :only_time)
    end +
    content_tag(:span, :class => (object[name].present? ? "hidden" : "visible") + " editable-field") do
      proposed_time = Time.parse("16:00:00")
      text_field_tag("#{name}_time", I18n.l(object[name].nil? ? proposed_time : object[name], :format => :only_time), :class => "input-mini", :data => { :provide => "typeahead", :items => "4", :source => a_day_at_intervals.to_s }, :autocomplete => 'off') +
      hidden_field_tag("#{resource_name}[#{name}(4i)]", object[name].nil? ? proposed_time.hour : object[name].hour) +
      hidden_field_tag("#{resource_name}[#{name}(5i)]", object[name].nil? ? proposed_time.min : object[name].min)
    end
    content.html_safe
  end

  def editable_select_field(name, object)
    resource_name = object.class.to_s.underscore

    content = content_tag(:span, :class => (object[name].present? ? "visible" : "hidden") + " editable-label") do
      I18n.t(object[name], :scope => [:activerecord, :attributes, object.class.to_s.underscore.to_sym, "#{name.to_s}_values"], :default => object[name])
    end +
    content_tag(:span, :class => (object[name].present? ? "hidden" : "visible") + " editable-field") do
      select_tag("#{resource_name}[#{name}]", options_for_select(object.send("#{name}_options_for_select"), object[name]))
    end
    content.html_safe
  end

  def fixed_date_field(name, object)
    content_tag(:span, :class => "editable-label") do
      object[name].nil? ? nil : I18n.l(object[name].to_date, :format => :default)
    end
  end

  def fixed_time_field(name, object)
    content_tag(:span, :class => "editable-label") do
      object[name].nil? ? nil : I18n.l(object[name], :format => :only_time)
    end
  end

  def fixed_select_field(name, object)
    if object[name].present?
      content_tag(:span, :class => "editable-label") do
        I18n.t(object[name], :scope => [:activerecord, :attributes, object.class.to_s.underscore.to_sym, "#{name.to_s}_values"], :default => object[name])
      end
    else
      nil
    end
  end

  def fixed_text_field(name, object)
    content_tag(:span, :class => "editable-label") do
      object[name]
    end
  end

  def a_day_at_intervals
    ((0..23).inject([]) {|r,i| r + (0..59).step(15).map{|j| "#{i.to_s.rjust(2, '0')}:#{j.to_s.rjust(2, '0')}" } })
  end

end
