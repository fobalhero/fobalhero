module StadiumsHelper
  def website_for_stadium(stadium)
    link_to stadium.website, stadium.website if stadium.website.present?
  end

  def description_for_stadium(stadium)
    simple_format(truncate(stadium.description, :length => 100))
  end
end
