/**
 * FobalHero Javascript library
 */

var FobalHero = FobalHero || {};
FobalHero.namespace = function (ns_string) {
  var parts = ns_string.split('.'),
  parent = FobalHero,
  i;
  // strip redundant leading global
  if (parts[0] === "FobalHero") {
    parts = parts.slice(1);
  }
  for (i = 0; i < parts.length; i += 1) {
    // create a property if it doesn't exist
    if (typeof parent[parts[i]] === "undefined") {
      parent[parts[i]] = {};
    }
    parent = parent[parts[i]];
  }
  return parent;
}

FobalHero.exec = function( controller, action ) {
  if ( controller !== "" && FobalHero[controller] && action !== "" && typeof FobalHero[controller][action] == "object" && typeof FobalHero[controller][action].init == "function" ) {
    FobalHero[controller][action].init();
  }
}

FobalHero.init = function() {
  var body = document.body,
  controller = body.getAttribute( "data-controller" ),
  action = body.getAttribute( "data-action" );
  FobalHero.preInit();
  FobalHero.exec( controller, action );
  FobalHero.postInit();
}

FobalHero.preInit = function() {
  if (($('body').height() + parseInt($('body').css('padding-top'))) < $(window).height()) {
    var difference = $(window).height() - ($('body').height() + parseInt($('body').css('padding-top')));
    $('.main-content-wrapper > .container').css('min-height', $('.main-content-wrapper > .container').height() + difference);
  }
}
FobalHero.postInit = function() {
  $('textarea.autogrow').autoGrowTextArea();
  window.setTimeout(function() {
    $(".navbar .alert-info").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
  }, 5000);
};

$( document ).ready( FobalHero.init );