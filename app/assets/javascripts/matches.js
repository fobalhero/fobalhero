FobalHero.namespace("matches");
FobalHero.matches.action_show = function() {
  var refreshCommentCount = 1;
  var baseCommentRefreshOffset = 10000; // 10 seconds
  var refreshCommentTimeout = null;

  var init = function() {
    prepareMatchFormInputs();
    prepareEditables();
    prepareComments();
    prepareInvites();
    prepareStadiumStars();
    prepareBadges();
    prepareNotifyChangesButton();
  }

  var prepareNotifyChangesButton = function() {
    $('a#notify_changes').click(function(e) {
      e.preventDefault();
      var $this = $(this);
      if (! $this.hasClass("disabled")) {
        notifyChanges($this);
      }
    });
  }

  var notifyChanges = function(element) {
    element.addClass("in-progress");
    $.ajax({
      type: 'POST',
      url: element.attr('href'),
      success: function() {
        element.addClass("disabled").removeClass("active").removeClass("in-progress");
      }}
    );
  }

  var prepareBadges = function() {
    $('#survey_container').on('click', 'ul.badges > li > a', toggleBadgeState);
    $('#survey_container').on('ajax:success', 'ul.badges > li > a', refreshBadgeStatus);
  }

  var refreshBadgeStatus = function(evt, data, status, xhr){
    var $badge = $(this).closest('li');
    $badge.replaceWith(xhr.responseText);
  }

  var toggleBadgeState = function() {
    $badge = $(this).closest('li');
    if ($badge.hasClass('voted')) {
      $badge.removeClass('voted');
    } else {
      $badge.addClass('voted');
    }
  }

  var prepareStadiumStars = function() {
    $('#survey_container').on('click', '.stadium .stars .star', toggleStarState);
    $('#survey_container').on('mouseenter', '.stadium .stars .star', toggleStarHoverIn);
    $('#survey_container').on('mouseleave', '.stadium .stars .star', toggleStarHoverOut);
  }

  var toggleStarState = function() {
    $star = $(this);
    $star.closest('.stars').find('.star').removeClass('active');
    $star.prevAll().addClass('active');
    $star.addClass('active');
  }

  var toggleStarHoverIn = function() {
    $star = $(this);
    $star.closest('.stars').find('.star').removeClass('hover');
    $star.prevAll().addClass('hover');
    $star.addClass('hover');
  }

  var toggleStarHoverOut = function() {
    $star = $(this);
    $star.closest('.stars').find('.star').removeClass('hover');
  }

  var restartCommentRefresh = function() {
    clearTimeout(refreshCommentTimeout);
    refreshCommentCount = 1;
    prepareNextCommentRefresh();
  }

  var prepareNextCommentRefresh = function() {
    var timeToNextRefresh = baseCommentRefreshOffset * refreshCommentCount;
    refreshCommentTimeout = setTimeout(
      function(){
        FobalHero.matches.action_show.fetchComments();
        refreshCommentCount += 1;
        prepareNextCommentRefresh();
      },
      timeToNextRefresh
    );
  }

  var fetchComments = function() {
    var $comments = $('#comments .comment').first();
    var commentId = "";
    var commentSide = "";
    $comments.each(function(){
      $this = $(this);
      commentId = $this.data().id;
      commentSide = $this.data().side;
    });
    $.ajax({
      url: $('#match_form').attr('action') + '/comments',
      data: {
        comment_id: commentId,
        side: commentSide
      },
      dataType: "script"
    });
  }

  var prepareComments = function() {
    prepareCommentForm();
    prepareNextCommentRefresh();
    fetchComments();
  }

  var prepareCommentForm = function() {
    $('form#new_comment textarea').keypress(function(e) {
      if (e.which == 13 && !e.shiftKey) {
        e.preventDefault();
        if ($('#comment_body').val() != "")
          $('form#new_comment').submit();
          $('#comment_body').attr("value", "");
      }
    });
  }

  var prepareMatchFormInputs = function() {
    prepareTimeSelector();
    prepareDatePicker();
    $('#match_team_one_name').change(function(){
      submitMatchForm();
    });
    $('#match_team_two_name').change(function(){
      submitMatchForm();
    });
    $('#match_match_type').change(function(){
      submitMatchForm();
    });
    $('#match_stadium_name').change(function(){
      submitMatchForm();
    });
  }

  var isPlayTimeValid = function() {
    return (/^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])?$/i.test($('#play_at_time').attr("value")));
  }

  var isPlayDateValid = function() {
    var currentDate = new Date();
    var day = $('#match_play_at_3i_').val();
    var month = $('#match_play_at_2i_').val();
    var year = $('#match_play_at_1i_').val();
    var date = new Date(year, month, day);
    return (currentDate < date);
  }

  var prepareTimeSelector = function() {
    $('#play_at_time').change(function(){
      var timeParts = $(this).val().split(":");
      var dateParts = $("#play_at_date").val().split("/");
      $('#match_play_at_4i_').val(Number(timeParts[0]));
      $('#match_play_at_5i_').val(Number(timeParts[1]));

      if(isPlayTimeValid()) {
        removeAttributeError($('#play_at_time'));        

      } else {
        showAttributeError($('#play_at_time'), "La hora ingresada es inválida");
      }

      submitMatchForm();
    });
  }

  var prepareDatePicker = function() {
    $( "#play_at_date" ).datepicker({
      dateFormat: "dd/mm/yy",
      onSelect: function(dateText, inst){
        var dateParts = dateText.split("/");
        var timeParts = $('#play_at_time').val().split(":");
        $('#match_play_at_3i_').val(dateParts[0]);
        $('#match_play_at_2i_').val(dateParts[1]);
        $('#match_play_at_1i_').val(dateParts[2]);

        if(isPlayDateValid()) {
          removeAttributeError($('#play_at_date'));          

        } else {
          showAttributeError($('#play_at_date'), "La fecha no puede ser en el pasado");
        }

        submitMatchForm();
      }
    });
  }

  var submitMatchForm = function() {
    var form = $("#match_form");
    updateMatchForm(form.serializeArray());

    if(errorsFound()) {
      return;   
    }    
    
    $.ajax({
      url: $('#match_form').attr('action'),
      type: 'PUT',
      data: form.serialize(),
      dataType: "script"
    });
  }

  var errorsFound = function() {
    return ($(".editable .editable-label.error").length > 0)
  }

  var removeAttributeError = function(input) {
    var label = input.closest(".editable").find(".editable-label");
    label.removeClass("error");
    label.tooltip("destroy");
  }

  var showAttributeError = function(input, error) {
    var label = input.closest(".editable").find(".editable-label");    
    label.attr("data-original-title", error);
    label.addClass("error");
    label.tooltip();
  }

  var updateMatchForm = function(formData) {
    $.each(formData, function(index, value) {
      var elementId = value["name"].replace(/\[/g, "_").replace(/\]/g, "").replace(/(\(|\))/g, "_");
      var input = $("#" + elementId);

      if ( (input.size() > 0) && (input.attr('type') != 'hidden') ) {
        if ( input.is("select") ) {          
          var text = input.find('option[value="' + value["value"] + '"]').html();
          input.closest(".editable").find(".editable-label").html(text);
        } else {
          input.closest(".editable").find(".editable-label").html(value["value"]);
        }
      }
    });
  }

  var prepareInvites = function() {
    handleInviteFormSubmit();
    $('#invite_button').click(handleInviteButtonClick);
  }

  var prepareEditables = function() {
    $('.editable .editable-label').click(
      function(){ toggleEditable($(this)) });

    $('.editable .editable-field input').keyup(
      function(e){ if(e.keyCode == 27) {
        this.value = $(this).closest('.editable').find('.editable-label').html();
        toggleEditable($(this)); }});

    $('.editable .editable-field select').keyup(
      function(e){ if(e.keyCode == 27) {
        toggleEditable($(this)); }});

    $('.editable .editable-field :input').keyup(
      function(e){ if(e.keyCode == 13) toggleEditable($(this)) });

    $('.editable .editable-field :input').focusout(
      function(){
        if($(this).closest(".editable-field").hasClass('visible')) {
          toggleEditable($(this)) }});

    $('.editable .editable-field select').change(
      function(){ toggleEditable($(this)) });

    $('#match_form :input[name=commit]').click(
      function(e) { e.preventDefault() } );
  }

  var toggleEditable = function($visibleEditable) {
    $editable = $visibleEditable.closest('.editable');
    $hidden = $editable.find('.hidden');
    $visible = $editable.find('.visible');
    $visible.fadeOut('fast', function() {
      $hidden.fadeIn('fast', function() {
        $hidden.find('input').focus();
      });
    });
    $visible.removeClass('visible').addClass('hidden');
    $hidden.removeClass('hidden').addClass('visible');
  }

  var handleInviteButtonClick = function(){
    $('#invite_friends_modal form input[type=submit]').show();
    $('#invite_friends_form_result_wrapper').hide();
    $('#invite_friends_form_wrapper').show();
    $('#invite_friends_form_wrapper form textarea').val('');
    displayPlayersThumbs();
    $('#invite_friends_modal').modal();
    return false;
  }

  var displayPlayersThumbs = function() {
    var $usersToInvite = $('#invite_friends_modal').find('.users-to-invite');
    var $selectedUsers = $("#friends_list input:checked");
    if ($selectedUsers.size() > 0)
      $usersToInvite.html("");
    else
      $usersToInvite.html("<li class='nobody'>Ningún amigo de la lista</li>");

    $selectedUsers.each(function(index, element){
      var $li = $('#sidebar_user_' + $(element).data().userId);
      $('<li/>', {
        title: $li.attr('title'),
        html: $('<img/>', {
          alt: $li.attr('title'),
          src: $li.find('img').attr('src')
        })
      }).addClass('user').appendTo($usersToInvite);
    });
  }

  var handleInviteFormSubmit = function() {
    $('#invite_friends_modal form').submit(function(e) {
      e.preventDefault();
      $('#invite_friends_modal form input[type=submit]').fadeOut();
      var $form = $( this );
      var comment = $form.find( 'textarea[name="invite[comment]"]' ).val();
      var match_id = $form.find( 'input[name="invite[match_id]"]' ).val();
      var url = $form.attr( 'action' );
      $('#invite_friends_form_wrapper').slideUp('slow');
      $('#invite_friends_form_result_wrapper').slideDown('slow');

      $.post( url, {
          match_id: match_id,
          comment: comment,
          user_ids: collectUsersToInvite(),
          emails: $('#invite_emails').val()
        },
        function(users) {
          var $undecided = $('ul.undecided');
          $.each(users, function(i, user){
            if ($undecided.find('li[data-user-id=' + user.id + ']').size() == 0) {
              var $li = $('<li class="user" style="display:none" data-user-id="' + user.id + '"><a href="/users/' + user.id + '" title="' + user.name + '"><img alt="player" src="' + user.avatar_thumb + '"></a></li>');
              $undecided.append($li);
              $li.fadeIn();
            }
          });
        }
      );
      startInviteVisualFeedback();
    });
}

var startInviteVisualFeedback = function() {
  moveUsersToStadium();
  $('#invite_friends_modal').on('hidden', function () {
    $('#invite_friends_modal').off('hidden');
    $.scrollTo('.invited h3', 1000);
  });
}

var collectUsersToInvite = function() {
  var user_ids = [];
  $("#friends_list input:checked").each(function(index, element) {
    user_ids.push($(element).data().userId);
  });
  return user_ids;
}

var moveUsersToStadium = function() {
  $("#friends_list input:checked").each(function(index, element) {
    var $li = $(this).closest('li');
    var href = $li.find('a').attr('href');
    var title = $li.find('span.user-name').text();
    var $undecided = $('ul.undecided');
    $undecided.append('<li class="user" title="' + title + '" data-user-id="' + $li.data().userId + '"><a href="' + href + '"><img alt="player" src="' + $li.data().thumb + '"></a></li>');
    $li.remove();
  });
}

return {
  init: init,
  fetchComments: fetchComments,
  restartCommentRefresh: restartCommentRefresh
}
}();

FobalHero.matches.action_update = FobalHero.matches.action_show;