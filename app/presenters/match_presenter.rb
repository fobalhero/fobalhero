class MatchPresenter
  def initialize(match, template)
    @match = match
    @template = template
  end

  def to_s
    @template.render :partial => 'matches/details', :locals => { :match => self }
  end

  def already_played?
    @match.already_played?
  end

  def play_at_time
    if @match.already_played?
      @template.fixed_time_field(:play_at, @match)
    else
      @template.editable_time_field(:play_at, @match)
    end
  end

  def play_at_date
    if @match.already_played?
      @template.fixed_date_field(:play_at, @match)
    else
      @template.editable_date_field(:play_at, @match)
    end
  end

  def match_type
    if @match.already_played?
      @template.fixed_select_field(:match_type, @match)
    else
      @template.editable_select_field(:match_type, @match)
    end
  end

  def stadium_name
    if @match.already_played?
      @template.fixed_text_field(:stadium_name, @match)
    else
      @template.editable_text_field(:stadium_name, @match)
    end
  end

  def team_one_name
    if @match.already_played?
      @template.fixed_text_field(:team_one_name, @match)
    else
      @template.editable_text_field(:team_one_name, @match)
    end
  end

  def team_two_name
    if @match.already_played?
      @template.fixed_text_field(:team_two_name, @match)
    else
      @template.editable_text_field(:team_two_name, @match)
    end
  end

  def team_one_proposed_score
    @match.team_one_proposed_score
  end

  def team_two_proposed_score
    @match.team_two_proposed_score
  end

end