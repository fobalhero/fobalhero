class MatchSurveyPresenter
  def initialize(match, user, template)
    @match = match
    @user = user
    @template = template
    @survey = @user.invitation_to_play(match).match_survey
  end

  def to_s
    @template.content_tag :div, :id => 'survey_container' do
      if played_step?
        @template.render :partial => 'matches/survey/played_step', :locals => { :match => @match, :survey => @survey }
      elsif no_played_step?
        @template.render :partial => 'matches/survey/review_step', :locals => { :match => @match, :survey => @survey }
      elsif score_step?
        if proposed_score_step?
          @template.render :partial => 'matches/survey/proposed_score_step', :locals => { :match => @match, :survey => @survey }
        else
          @template.render :partial => 'matches/survey/score_step', :locals => { :match => @match, :survey => @survey }
        end
      elsif goals_step?
        @template.render :partial => 'matches/survey/goals_step', :locals => { :match => @match, :survey => @survey }
      elsif qualify_step?
        @template.render :partial => 'matches/survey/qualify_step', :locals => { :match => @match, :survey => @survey }
      else
        @template.render :partial => 'matches/survey/review_step', :locals => { :match => @match, :survey => @survey }
      end
    end
  end

  def qualify_step?
    @match.pending_review?
  end

  def proposed_score_step?
    (@match.match_surveys - [@survey]).each do |survey|
      return true if survey.team_one_score.present?
    end
    false
  end

  def played_step?
    @survey.nil? or @survey.played.nil?
  end

  def no_played_step?
    @survey.played == false
  end

  def score_step?
    @survey.team_one_score.nil?
  end

  def goals_step?
    @survey.goals.nil?
  end
end