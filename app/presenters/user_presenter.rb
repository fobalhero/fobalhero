class UserPresenter
  def initialize(user, template)
    @user = user
    @template = template
  end

  def goals_badge(match)
    goals = @user.goals(match)
    return "" if goals == 0
    @template.content_tag :li, :class => 'goals' do
      goals.to_s
    end
  end

  def yellow_card_badge(match)
    @template.content_tag(:li, nil, :class => 'yellow-card')
  end

  def red_card_badge(match)
    @template.content_tag(:li, nil, :class => 'red-card')
  end
  
  def doping_badge(match)
    @template.content_tag(:li, nil, :class => 'doping')
  end
  
  def man_of_the_match_badge(match)
    @template.content_tag(:li, nil, :class => 'man-of-the-match')
  end

  def cold_chest_badge(match)
    # cold chest?? sisi ya se, pero me parecio gracioso :)
    @template.content_tag(:li, nil, :class => 'cold-chest')
  end
  
  def betrayer_badge(match)
    @template.content_tag(:li, nil, :class => 'betrayer')
  end

end