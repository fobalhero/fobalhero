class Stadium < ActiveRecord::Base
  attr_accessible :description, :lat, :lon, :name, :phone_one, :phone_two, :address, :logo, :website, :email
  
  has_attached_file :logo,
    :styles => {
      :medium => "300x300>",
      :thumb => "50x50#",
      :mini => "24x24#"
    }

  validates :name, :description, :phone_one, :presence => true
  validates_format_of :email, :with => /@/

  has_many :matches
end
