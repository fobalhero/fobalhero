class Match < ActiveRecord::Base
  include FobalHero::Match::Survey
  include FobalHero::Match::Notifications
  include FobalHero::Match::Stadium
  include FobalHero::Match::State
  include FobalHero::Match::User
  include FobalHero::Match::Utils

  MATCH_TYPES = [:five, :seven, :nine, :eleven]
  IMPORTANT_CHANGES = ["play_at","stadium_name"]

  attr_accessible :play_at, :team_one_name, :team_two_name, :match_type, :stadium, :stadium_name

  serialize :match_changes, Hash

  validate :play_at_cant_be_in_the_past

  has_many :invitations_to_play, :dependent => :destroy, :class_name => 'InvitationToPlay' do
    def accepted
      where(:accepted => true)
    end

    def declined
      where(:accepted => false)
    end

    def pending
      where(:accepted => nil)
    end
  end

  has_many :comments, :as => :commentable, :dependent => :destroy

  after_create :create_host_invitation
  before_save :toggle_draft_status, :store_match_changes

  def self.past(user)
    where('matches.play_at < ?', (Time.now - 10.days)).
    joins(:invitations_to_play).
    where('invitations_to_play.user_id = ?', user.id).
    order(:play_at)
  end

  def self.pending_review(user)
    where(:play_at => (Time.now - 10.days)..(Time.now)).
    joins(:invitations_to_play).
    where('invitations_to_play.user_id = ?', user.id).
    where('invitations_to_play.accepted = ?', true).
    order(:play_at)
  end

  def self.upcoming(user)
    match_ids = where("play_at > ?", Time.now).
      where('matches.host_id = ?', user.id).
      pluck('matches.id')

    match_ids << unscoped.joins(:invitations_to_play).
      where('invitations_to_play.user_id = ?', user.id).
      where('matches.play_at > ?', Time.now).
      pluck('matches.id')

    match_ids << unscoped.joins(:invitations_to_play).
      where('invitations_to_play.user_id = ?', user.id).
      where('invitations_to_play.accepted = ?', true).
      where('matches.play_at IS ?', nil).
      pluck('matches.id')

    self.unscoped.where(:id => match_ids.uniq).order(:play_at)
  end

  def self.waiting_response(user)
    self.joins(:invitations_to_play).
      where('invitations_to_play.user_id = ?', user.id).
      where('invitations_to_play.accepted IS ?', nil).
      where('matches.play_at IS ? OR matches.play_at > ?', nil, Time.now).
      order(:play_at)
  end

  def self.next(user)
    self.upcoming(user).order('matches.play_at DESC').limit(1).first
  end

  def self.prev
    where('play_at <= ?', Time.zone.now).order(:play_at).limit(1).first
  end

  def self.draft(user)
    where(:is_draft => true).where(:host_id => user.id).first
  end

  def already_played?
    return false if self.is_draft
    return false if self.play_at.nil?
    return true if self.play_at < Time.now
    false
  end

  def pending_review?
    already_played? and !is_closed?
  end

  def match_type_options_for_select
    MATCH_TYPES.map {|type| [I18n.t(type, :scope => [:activerecord, :attributes, :match, :match_type_values]), type]}
  end

  def is_closed?
    (self.play_at < Time.now - 10.days) rescue false
  end

  def team_one_proposed_score
    all = self.match_surveys.map { |survey| survey.team_one_score }
    proposed_score = 0
    all.uniq.each do |score|
      proposed_score = score if all.count(score) > all.count(proposed_score)
    end
    proposed_score
  end

  def team_two_proposed_score
    all = self.match_surveys.map { |survey| survey.team_two_score }
    proposed_score = 0
    all.uniq.each do |score|
      proposed_score = score if all.count(score) > all.count(proposed_score)
    end
    proposed_score
  end

  def sum_users_goals
    self.match_surveys.inject(0) {|result, survey| survey.goals.nil? ? result : result + survey.goals }
  end

  def number_of_players_in_stadium
    case self.match_type
    when 'five'
      return 10
    when 'seven'
      return 14
    when 'nine'
      return 18
    when 'eleven'
      return 22
    end
  end

private

  def create_host_invitation
    invitation = InvitationToPlay.new
    invitation.match = self
    invitation.host = self.host
    invitation.user = self.host
    invitation.message = "Invitado por ser el organizador"
    invitation.accepted = true
    invitation.save!
    invitation.accepted!
  end

  def toggle_draft_status
    if self.invitations_to_play.accepted.count > 1
      self.is_draft = false
    else
      self.is_draft = true
    end
    return true
  end

  def store_match_changes
    return nil if self.is_draft?
    important_changes = self.changes.select{|key,value| IMPORTANT_CHANGES.include? key}
    self.match_changes = self.match_changes.merge important_changes
    self.match_changes.delete("match_changes")
  end

  def play_at_cant_be_in_the_past
    return true if self.play_at.nil?
    unless self.play_at > Time.now
      errors.add(:play_at, :past_play_at)
    end
  end
end
