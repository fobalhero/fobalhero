class Comment < ActiveRecord::Base
  include FobalHero::Comment::RandomMessage

  attr_accessible :body

  belongs_to :commentable, :polymorphic => true
  belongs_to :user
  delegate :name, :to => :user, :allow_nil => true, :prefix => true

  before_save :trim_strings


  scope :newer_first, order('id ASC')
  scope :older_first, order('id DESC')

private

  def trim_strings
    self.body = self.body.strip
  end

end
