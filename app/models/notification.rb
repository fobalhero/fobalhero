class Notification < ActiveRecord::Base
  belongs_to :user
  attr_accessible :message, :user_id, :match_id, :notification_type

  scope :for_user, lambda {|user| where(:user_id => user.id)}
  scope :unread, where(:read => false)
  scope :latest_first, order('updated_at DESC')

  validates :notification_type, :match_id, :user_id, :message, :presence => true

  TYPE_DESERTED = 0
  TYPE_COMMENT = 1

  def read?
    self.read
  end
end
