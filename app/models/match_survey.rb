class MatchSurvey < ActiveRecord::Base
  belongs_to :invitation_to_play, :class_name => 'InvitationToPlay'
  has_many :match_survey_badges, :dependent => :destroy

  validates :team_one_score, :numericality => { :only_integer => true }, :if => 'self.team_one_score.present?'
  validates :team_two_score, :numericality => { :only_integer => true }, :if => 'self.team_two_score.present?'
  validates :goals, :numericality => { :only_integer => true }, :if => 'self.goals.present?'
  validates :stadium_stars,
    :numericality => { :only_integer => true, :greater_than_or_equal_to => 1, :less_than_or_equal_to => 5 },
    :if => 'self.stadium_stars.present?'

  scope :by_user, lambda {|user| joins(:invitation_to_play).where('invitations_to_play.user_id = ?', user.id)}
  scope :by_match, lambda {|match| joins(:invitation_to_play).where('invitations_to_play.match_id =?', match.id)}

  def did_vote_badge_for_player?(type, player, exclude)
    q = self.match_survey_badges.where('user_id = ? AND badge_type = ?', player.id, type)
    q = q.where('user_id NOT IN (?)', exclude)
    q.exists?
  end

  def match_id
    self.invitation_to_play.match_id
  end

  def match
    self.invitation_to_play.match
  end
end

