class MatchSurveyBadge < ActiveRecord::Base
  belongs_to :match_survey
  belongs_to :user
  attr_accessible :match_survey_id, :user_id, :type
  validates :match_survey, :user, :badge_type, :presence => true
end
