class InvitationToPlay < ActiveRecord::Base
	self.table_name = "invitations_to_play"
  attr_accessible :match, :user, :host, :message

  belongs_to :match
  belongs_to :user
  belongs_to :host, :class_name => 'User'
  has_one :match_survey, :dependent => :destroy

  delegate :id, :name, :avatar, :to => :user, :allow_nil => true, :prefix => true
  delegate :id, :to => :match, :allow_nil => true, :prefix => true

  validates :match, :user, :host, :presence => true

  after_create :send_invitation
  after_save :force_match_update

  def accepted!
    comment_user_accepted unless self.accepted?
    self.accepted = true
    save!
  end

  def declined!
    if self.accepted?
      comment_user_deserted
      notify_user_deserted unless match.is_draft
    end
    self.accepted = false
    save!
  end

  def accepted?
    self.accepted === true
  end

  def declined?
    self.accepted === false
  end

  def pending?
    self.accepted === nil
  end

  def self.invite(users, match, host, message)
    users.each do |user|
      invitation = InvitationToPlay.find_or_initialize_by_match_id_and_host_id_and_user_id(match.id, host.id, user.id)
      next if invitation.accepted?
      invitation.message = message
      invitation.accepted = nil
      invitation.save
    end
  end

private

  def send_invitation
    UserMailer.invite_to_play(self.id).deliver if self.accepted.nil? and self.host_id != self.user_id
  end

  def notify_user_deserted
    recipients = match.users.going.pluck :email
    recipients.delete(self.user.email)
    UserMailer.notify_user_deserted(self.id, recipients).deliver
  end

  def comment_user_accepted
    comment = Comment.new(:body => Comment.random_accept_message)
    comment.commentable_id = self.match_id
    comment.commentable_type = "Match"
    comment.user_id = self.user_id
    comment.save!
  end

  def comment_user_deserted
    comment = Comment.new(:body => Comment.random_desert_message)
    comment.commentable_id = self.match_id
    comment.commentable_type = "Match"
    comment.user_id = self.user_id
    comment.save!
  end

  def force_match_update
    self.match.save!
  end
end
