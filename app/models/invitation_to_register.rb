class InvitationToRegister < ActiveRecord::Base
  self.table_name = "invitations_to_register"
  attr_accessible :host_id, :user_id, :accepted

  belongs_to :user
  belongs_to :host, :class_name => 'User'

  validates :user, :host, :presence => true
  after_create :send_invitation

  def accepted?
    self.accepted === true
  end

  def pending?
    self.accepted === nil
  end

  def accepted!
    self.update_attributes :accepted => true
    send_notification_to_host
    send_welcome_to_user
  end

private
  def send_invitation
    UserMailer.invite_to_register(self).deliver
  end

  def send_notification_to_host
    UserMailer.notify_host_user_accepted(self).deliver
  end

  def send_welcome_to_user
    UserMailer.say_welcome_to_user(self).deliver
  end
end
