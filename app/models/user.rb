class User < ActiveRecord::Base

  include FobalHero::User::Match
  include FobalHero::User::Facebook
  include FobalHero::User::InvitationToPlay
  include FobalHero::User::InvitationToRegister
  include FobalHero::User::Friendship

  attr_accessible :email,
    :password,
    :password_confirmation,
    :remember_me,
    :name,
    :avatar,
    :provider,
    :uid

  devise :database_authenticatable,
    :registerable,
    :confirmable,
    :recoverable,
    :rememberable,
    :trackable,
    :validatable,
    :omniauthable,
    :token_authenticatable

  validates :name, :presence => true

  has_attached_file :avatar,
    :styles => {
      :medium => "300x300>",
      :thumb => "50x50#",
      :mini => "24x24#"
    }

  has_many :comments, :dependent => :destroy

  scope :newer_first, order(:created_at)
  scope :last_logged_first, where('current_sign_in_at IS NOT NULL').order('users.current_sign_in_at DESC')
  scope :never_logged, where('current_sign_in_at IS NULL').order(:created_at)

  before_validation :set_name_from_email, :if => 'self.name.blank?'

  def avatar_thumb
    self.avatar.url(:thumb)
  end

  def prepare_to_set_password!
    self.reset_password_token = User.reset_password_token
    self.reset_password_sent_at = Time.now
    self.save!
  end

private

  def set_name_from_email
    self.name = self.email[/[^@]+/]
  end

end
