class UserMailer < ActionMailer::Base

  def invite_to_play(invitation_id)
    @invitation = InvitationToPlay.find invitation_id
  	@host = @invitation.host
    @user = @invitation.user
    @user.ensure_authentication_token!
    @match = @invitation.match
    @message = @invitation.message
    mail :to => @user.email,
      :subject => I18n.t(:you_are_invited_to_play,
        :scope => [:emails, :subjects, :invitations_to_play])
  end

  def invite_to_register(invitation)
    @invitation = invitation
    @host = invitation.host
    @user = invitation.user
    mail :to => @user.email,
      :subject => I18n.t(:you_are_invited_to_register,
        :scope => [:emails, :subjects, :invitations_to_register], :host_name => @host.name)
  end

  def notify_host_user_accepted(invitation)
    @invitation = invitation
    @host = invitation.host
    @user = invitation.user
    mail :to => @host.email,
      :subject => I18n.t(:user_accepted_invitation,
        :scope => [:emails, :subjects, :invitations_to_register], :user_name => @user.name)
  end

  def notify_user_deserted(invitation_id, recipients)
    @invitation = InvitationToPlay.find invitation_id
    @user = @invitation.user
    @recipients = recipients
    @match = @invitation.match
    mail :to => @recipients,
      :subject => I18n.t(:user_deserted,
        :scope => [:emails, :subjects, :invitations_to_play], :user_name => @user.name)
  end

  def say_welcome_to_user(invitation)
    @invitation = invitation
    @host = invitation.host
    @user = invitation.user
    mail :to => @user.email,
      :subject => I18n.t(:welcome_new_user,
        :scope => [:emails, :subjects, :invitations_to_register])
  end
end
