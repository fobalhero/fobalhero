class MatchMailer < ActionMailer::Base
  include MailerHelper
  helper :mailer

  def notify_match_changes(match_id, changes, recipients)
    @match = Match.find match_id
    @host = @match.host
    @changes = changes

    mail :to => recipients, :subject => subject_for_match_changes(@changes)
  end
end
